# talks

Curated list of talks I've given at conferences and community events.

## Table of contents

| Date | Title | Event | Location | Links |
| --- | --- | --- | --- | --- |
| 2022-12-16 | 5 Guiding Principles To Practical Digital Ecodesign (620 kB) | [Apidays Paris 2022](https://www.apidays.global/paris/) | Paris, FR | [slides](./2022_12_16%20-%20Apidays%20Paris%20-%205%20Guiding%20Principles%20To%20Practical%20Digital%20EcoDesign.pdf) (620 kB), [video](https://www.youtube.com/watch?v=HXxW9duQgbo) (YouTube) |
| 2019-05-25 | Bocadillo, or how I built an open source Python async web framework | [PyConWeb 2019](https://pyconweb.com/) | Munich, DE | [slides](./2019_05_25-bocadillo_pyconweb2019.pdf) (2.1 MB) |
